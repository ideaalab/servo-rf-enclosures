# 4 Servo Controller RF electronics enclosure

3D printable tool-less box intended for using with IDEAA Lab's [4 Servo Controller RF](https://ideaalab.com/en/shop/servomotors/controllers/4-servo-controller-rf-detail)